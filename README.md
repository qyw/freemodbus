See official site [freemodbus.org](http://www.freemodbus.org) and [SourceForge](https://sourceforge.net/projects/freemodbus.berlios/).

About
=========
FreeMODBUS is a free implementation of the popular Modbus protocol specially targeted for embedded systems. Modbus is a popular network protocol in the industrial manufacturing environment. A Modbus communication stack requires two layers. The Modbus Application Protocol which defines the data model and functions and a Network layer. In its current version FreeMODBUS provides an implementation of the Modbus Application Protocol v1.1a and supports RTU/ASCII transmission modes defined in the Modbus over serial line specification 1.0. Since version 0.7 FreeModbus also supports Modbus TCP defined in Modbus Messaging on TCP/IP Implementation Guide v1.0a. It is licensed under the BSD[1] which permits its usage in commercial environments. The following Modbus functions are currently supported:

    Read Input Register (0x04)
    Read Holding Registers (0x03)
    Write Single Register (0x06)
    Write Multiple Registers (0x10)
    Read/Write Multiple Registers (0x17)
    Read Coils (0x01)
    Write Single Coil (0x05)
    Write Multiple Coils (0x0F)
    Read Discrete Inputs (0x02)
    Report Slave ID (0x11)

The implementation is based upon the most recent standards and should be fully standard compliant. Receiving and transmitting of Modbus RTU/ASCII frames is implemented as a state machines which is driven by callbacks from the hardware abstraction layer. This makes porting to new platforms easy. If a frame is complete it is passed to the Modbus Application Layer where its content is inspected. Hooks are available in the Application Layer to add new Modbus functions.
If Modbus TCP is used the porting layer must send an event to the protocol stack if a new frame is ready for processing. The protocol stack then calls a function which returns the received Modbus TCP frame and processes it. If valid a response is created and the porting layer is supplied with the Modbus response. It should then send the response back to the client.

[1]:This excludes the demo applications which might be licensed differently.


Information
-----------
Commercial support is available from [Embedded Solutions](https://web.archive.org/web/20151125064751/http://www.embedded-solutions.at/). 